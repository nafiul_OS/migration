const axios = require('axios');
const { writeJsonInFile } = require('./index');

// const COMPANY_URL = "http://159.203.105.25:8080/api/v/0.0.1/secured/companies/create"
// const GENERIC_URL = "http://159.203.105.25:8080/api/v/0.0.1/secured/generics/create"
// const PRAGNANCY_CATEGORY = "http://159.203.105.25:8080/api/v/0.0.1/secured/pregnancy-categories/create"

const COMPANY_URL = "http://192.168.0.115:8080/api/v/0.0.1/secured/companies/create";
const GENERIC_URL = "http://192.168.0.115:8080/api/v/0.0.1/secured/generics/create";
const PRAGNANCY_CATEGORY = "http://192.168.0.115:8080/api/v/0.0.1/secured/pregnancy-categories/create";

const PRODUCT_URL = "http://192.168.0.115:8080/api/v/0.0.1/secured/products/create";

let companies = require("./result/t_company_name.json");
let generics = require("./final/generics.json");
let products = require("./final/products.json");
let pregnancyCategories = require("./result/t_pregnancy_category.json");

const options = { headers: { "Authorization": `Bearer eyJhbGciOiJIUzUxMiJ9.eyJDVVJSRU5UX1VTRVIiOnsidXNlcm5hbWUiOiJhZG1pbkBtYWlsLmNvbSIsInBlcm1pc3Npb25NYXNrIjoyNTUyMTE3NzUxOTA3MDUwMTc1MjU3NDM0ODQ3OTYxMTMyMDY3Njd9LCJzdWIiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTU3OTQwNjI1NCwiZXhwIjoxNTc5NDkyNjU0fQ.RPE1DR7r__pAOBo5RhujMqjotRJRwf9Cj-F_jJmFYaYA7w6o7XPkqyyFbwhBzU400oV0fr4VToaBgi0zgdoBOQ` } }


/* ===================== Preg cate generic =============== */

// let promise = new Promise((resolve, reject) => {

//     pregnancyCategories.forEach(c => {

//         // c.genericsName = c.generic_name;
//         c.pregnancyName = c.pregnancy_name;
//         c.pregnancyDesc = c.pregnancy_description;

//         axios.post(PRAGNANCY_CATEGORY, c, options).then(res => {

//             generics.map(generic => {
//                 if (generic.pregnancyCategory.id === c.pregnancy_id + 1) {
//                     generic.pregnancyCategory = {
//                         id: res.data.id
//                     }
//                 }
//                 return generic;
//             })

//         }).catch(error => {
//             reject("Failed");
//             console.log("error => ", error)
//         })
//     })

//     setTimeout(() => {
//         resolve(generics);
//     }, 5000)
// })

// promise.then((res) => {
//     // console.log(res);
//     writeJsonInFile("./final/generics-with-preg.json", res);

// }, (error) => {
//     console.log(error);
// })

/* ===================== Product Company =============== */

// let promise1 = new Promise((resolve, reject) => {

//     companies.forEach(c => {

//         c.companyName = c.company_name;

//         axios.post(COMPANY_URL, c, options).then(res => {

//             products.map(product => {
//                 if (product.company_id === c.company_id) {
//                     product.company = {
//                         id: res.data.id
//                     }
//                 }
//                 return product;
//             })

//         }).catch(error => {
//             reject("Failed");
//             console.log("error => ", error)
//         })
//     })

//     setTimeout(() => {
//         resolve(products);
//     }, 5000)
// })

// promise1.then((res) => {
//     // console.log(res);
//     writeJsonInFile("./final/products-with-company.json", res);

// }, (error) => {
//     console.log(error);
// })

/* ==================== Product Generics ================= */

// let productsWithCompany = require("./final/products-with-company.json");
// let newGenerics = require("./final/generics-with-preg.json");

// let promise2 = new Promise((resolve, reject) => {

//     newGenerics.forEach(g => {

//         axios.post(GENERIC_URL, g, options).then(res => {

//             productsWithCompany.map(product => {
//                 if (product.generic_id === g.id) {
//                     product.generics = {
//                         id: res.data.id
//                         // id: g.id
//                     }
//                 }
//                 return product;
//             })

//         }).catch(error => {
//             // reject("Failed");
//             console.log("error => ", error)
//         })
//     })

//     setTimeout(() => {
//         resolve(productsWithCompany);
//     }, 5000)
// })

// promise2.then((res) => {
//     console.log(res);
//     writeJsonInFile("./final/products-with-company-generics.json", res);
// }, (error) => {
//     console.log(error);
// })

/*  */

const productFinal = require("./final/products-with-company-generics.json");

productFinal.forEach(async product => {
    try {
        const res = await axios.post(PRODUCT_URL, product, options);
        console.log(res.data)
    } catch (error) {
        console.log(error)
    }


})