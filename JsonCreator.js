const csv = require('csv-parser')
const fs = require('fs')

const writeJsonInFile = (fileName, data) => {
    fs.writeFile(`${fileName}`, JSON.stringify(data), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
}

// const csvToJSON = (filePath, desFilePath) => {
//     const results = [];
//     fs.createReadStream(filePath)
//         .pipe(csv())
//         .on('data', (data) => results.push(data))
//         .on('end', () => {

//             writeJsonInFile(desFilePath, results)
//         });
// }

// const readFiles = (dirname, onError) => {
//     fs.readdir(dirname, (err, fileNames) => {
//         if (err) {
//             onError(err);
//             return;
//         }
//         fileNames.forEach((fileName) => {
//             csvToJSON(`./csvs/${fileName}`, "./result/" + fileName.replace("csv", "json"));
//         });
//     });
// }

// // readFiles("./csvs");


// /* Thana & District */
// let district = require("./result/district.json");
// let thana = require("./result/thana.json");

// const newThana = thana.map(async t => {

//     t.thanaName = t.name;
//     t.id = t.id + 1;

//     const dist = district.find(d => d.id === t.dist_id);

//     t.district = {
//         districtName: dist.name
//     };

//     delete t.name;
//     delete t.dist_id;

//     return t;
// });

// // writeJsonInFile("./final/thana.json", newThana);


// /* Generics */

// let generics = require("./result/t_drug_generic.json");
// let pregnancyCategoryList = require("./result/t_pregnancy_category.json");

// const newGenerics = generics.map(g => {

//     g.id = g.generic_id + 1;
//     g.genericsName = g.generic_name;
//     g.adultDose = g.adult_dose;
//     g.childDose = g.child_dose;
//     g.renalDose = g.renal_dose;
//     g.contradictionIndication = g.contra_indication;
//     g.modeOfAction = g.mode_of_action;
//     g.pregnancyCategoryNote = g.pregnancy_category_note;
//     g.sideEffects = g.side_effect;

//     const preg = pregnancyCategoryList.find(p => p.pregnancy_id === g.pregnancy_category_id);

//     g.pregnancyCategory = {
//         id: preg.pregnancy_id + 1,
//         pregnancyName: preg.pregnancy_name,
//         pregnancyDesc: preg.pregnancy_description
//     }

//     delete g.generic_id;
//     delete g.generic_name;
//     delete g.adult_dose;
//     delete g.child_dose;
//     delete g.renal_dose;
//     delete g.contra_indication;
//     delete g.mode_of_action;
//     delete g.pregnancy_category_note;
//     delete g.side_effect;

//     return g;
// });

// // writeJsonInFile("./final/generics.json", newGenerics);

// // let newGenericsList = require("./final/generics.json");
// // let products = require("./result/t_drug_brand.json");


// const COMPANY_URL = "http://192.168.1.106:8080/api/v/0.0.1/secured/companies/create"
// const GENERIC_URL = "http://192.168.1.106:8080/api/v/0.0.1/secured/generics/create"
// const options = { headers: { "Authorization": `Bearer eyJhbGciOiJIUzUxMiJ9.eyJDVVJSRU5UX1VTRVIiOnsidXNlcm5hbWUiOiJhZG1pbkBtYWlsLmNvbSIsInBlcm1pc3Npb25NYXNrIjozMDczNX0sInN1YiI6ImFkbWluQG1haWwuY29tIiwiaWF0IjoxNTc3NjAzMDAxLCJleHAiOjE1Nzc2ODk0MDF9.R3b867wykK98oKGU89mLu0tqiGuNa_MH3kYzV7FXe4c2FIM2ljSGO6AzW3vlJ03OrDQvNplW52NXrRWjQmlutg` } }
// const axios = require('axios');

// const productList = products.map(p => {

//     // p.generics = newGenericsList.find(g => g.id === p.generic_id + 1);

//     // const com = companies.find(c => c.company_id === p.company_id);

//     // if (com) com.id = com.company_id;
//     // if (com) com.companyName = com.company_name;

//     // p.company = com || {
//     //     companyName: "unknown"
//     // };

//     p.categories = [];

//     p.productName = p.brand_name;
//     // p. = p.form
//     // p. = p.strength
//     p.unitPrice = p.price;
//     p.countryOfOrigin = {
//         id: 1,
//         countryName: "Bangladesh"
//     }

//     p.packSize = p.packsize;
//     if (p.isFav !== 0) {
//         p.packSize = `${p.packSize} ${p.isFav}`;
//     }

//     // delete p.company_id;
//     delete p.brand_id;
//     delete p[''];
//     // delete p.generic_id;
//     delete p.brand_name;
//     delete p.price;
//     delete p.packsize;
//     delete p.isFav;

//     return p;
// });

// writeJsonInFile("./final/products.json", productList);



module.exports = { writeJsonInFile };
